import { createSlice } from "@reduxjs/toolkit";

const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todos: [],
        favTodos: [],
        totalQuantity: 0
    },
    reducers: {
        fetchTodos(state, action) {
            const newTodos = action.payload;
            state.todos = newTodos;
            // const newFavTodos = state.todos.filter(item => item.favourite === "true");
            state.favTodos = newTodos.filter(item => item.favourite === 1);
            // console.log(newTodos.filter(item => item.favourite === "true"));
            state.totalQuantity = state.todos.length;
        },
        addTodo(state, action) {
            const newTodos = action.payload;
            // console.log(newTodos)
            state.todos.push(newTodos);
            state.totalQuantity++;
        },
        deleteTodo(state, action) {
            const itemId = action.payload.id;
            state.totalQuantity--;
            state.todos = state.todos.filter(item => item.id !== itemId);
            state.favTodos = state.favTodos.filter(item => item.id !== itemId);
        },
        updateTodo(state, action) {
            const itemId = action.payload.id;
            const existingItem = state.todos.find(item => item.id === itemId);
            existingItem.title = action.payload.title;
            existingItem.description = action.payload.description;
            if (existingItem.favourite === 1) {
                const existingFavItem = state.favTodos.find(item => item.id === itemId);
                existingFavItem.title = action.payload.title;
                existingFavItem.description = action.payload.description;
            }
        },
        addFavourite(state, action) {
            const itemId = action.payload;
            const selectedItem = state.todos.find(item => item.id === itemId);
            selectedItem.favourite = 1;
            // console.log(selectedItem.favourite);
            state.favTodos.push(selectedItem);
            // state.favTodos = state.todos.slice(item => item.favourites === true);
        },
        removeFavourite(state, action) {
            const itemId = action.payload;
            const selectedItem = state.todos.find(item => item.id === itemId);
            selectedItem.favourite = 0;
            state.favTodos = state.favTodos.filter(item => item.id !== itemId);
        }
    }
})

export const todoActions = todoSlice.actions;
export default todoSlice;