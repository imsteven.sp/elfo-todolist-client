import { render, fireEvent } from '@testing-library/react'
import NewTodo from '../components/Todos/NewTodo'
import { Provider } from 'react-redux';
import store from '../store';

it('checkTitleRender', () => {
    const { queryByTestId } = render(<Provider store={store}><NewTodo /></Provider>);
    const input = queryByTestId("testTitle");
    expect(input).toBeTruthy();
});

describe('changeInput', () => {
    it('onChange', () => {
        const { queryByTestId } = render(<Provider store={store}><NewTodo /></Provider>);
        const input = queryByTestId("testTitle");
        fireEvent.change(input, { target: { value: 'Start up ideas' } });
        expect(input.value).toBe('Start up ideas');
    });
});

it('checkDescRender', () => {
    const { queryByTestId } = render(<Provider store={store}><NewTodo /></Provider>);
    const input = queryByTestId("testDescription");
    expect(input).toBeTruthy();
});

describe('changeInput', () => {
    it('onChange', () => {
        const { queryByTestId } = render(<Provider store={store}><NewTodo /></Provider>);
        const input = queryByTestId("testDescription");
        fireEvent.change(input, { target: { value: 'Some notes here ...' } });
        expect(input.value).toBe('Some notes here ...');
    });
});

