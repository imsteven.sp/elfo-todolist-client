import { render, fireEvent } from '@testing-library/react'
import Button from '../components/ui/Button'

it('checkButtonReder', () => {
    const { queryByTestId } = render(<Button />);
    const btn = queryByTestId("testButton");
    expect(btn).toBeTruthy();
});