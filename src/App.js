import React, { useEffect } from 'react';
import axios from 'axios';
import Layout from './components/Layout/Layout';
import AllTodos from './components/Todos/AllTodos';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import FavouriteTodos from './components/Todos/FavouriteTodos';
import NewTodo from './components/Todos/NewTodo';
import { useDispatch } from 'react-redux';
import { todoActions } from './store/todo-slice';

const App = () => {
  const dispatch = useDispatch();

  // //GET API
  // const getData = () => {
  //   axios.get('http://localhost:9001/tasks')
  //     .then((response) => dispatch(todoActions.fetchTodos(response.data.tasks)));
  //   console.log('Fetching Data from API')
  // };

  useEffect(() => {
    axios.get('http://localhost:9001/tasks')
      // .then((response) => console.log(response.data.tasks))
      .then((response) => dispatch(todoActions.fetchTodos(response.data.tasks)));
    console.log('Fetching Data from API')
  }, [dispatch]);

  return (
    <Router>
      <Layout>
        <Switch>
          <Route path='/' exact>
            <AllTodos />
          </Route>
          <Route path='/new'>
            <NewTodo />
          </Route>
          <Route path='/favourite'>
            <FavouriteTodos />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
}
export default App;
