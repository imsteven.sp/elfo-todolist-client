import React from 'react'
import './Header.scss'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import SideBarWrapper from './SideBarWrapper';

const Header = () => {
    const cartQuantity = useSelector(state => state.todo.totalQuantity)
    return (
        <header>
            <div className="appbar">
                <Link to='/' className="appbar__logo">ELFO <span style={{ color: '#ffc7c8' }}>NOTES</span></Link>
                <nav>
                    <SideBarWrapper/>
                    {/* <IconButton color="inherit" className="appbar__icon" size="large">
                        <MenuOutlinedIcon />
                    </IconButton> */}
                    <ul className="appbar__nav-items">
                        <Link to='/' className="appbar__nav-item">
                            <li>My Todos <span className="appbar__badge">{cartQuantity}</span></li>
                        </Link>
                        <Link to='/new' className="appbar__nav-item">
                            <li>New Todo</li>
                        </Link>
                        <Link to='/favourite' className="appbar__nav-item">
                            <li>Favourites</li>
                        </Link>
                    </ul>
                </nav>
            </div>
        </header>
    )
}

export default Header
