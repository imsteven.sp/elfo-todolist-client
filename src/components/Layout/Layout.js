import React from 'react'
import Header from './Header'
import PropTypes from 'prop-types'

const Layout = ({children}) => {
    return (
        <React.Fragment>
            <Header />
            <main style={{ paddingTop: '15vh', backgroundColor: 'var(--color-primary)' }}>{children}</main>
        </React.Fragment>
    )
}

Layout.propTypes = {
    children: PropTypes.element
}

export default Layout
