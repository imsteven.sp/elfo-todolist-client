import React from 'react'
import PropTypes from 'prop-types'
import './button.scss'

// eslint-disable-next-line react/prop-types
const Button = ({ label, size = "md", onClick, className }) => {

    let scale = 50;
    if (size === "sm") scale = 45;
    if (size === "lg") scale = 80;
    const style = {
        width: `calc(${scale}px + 2vmin)`,
        height: `calc(${scale}px + 2vmin)`,
    }

    let textScale = 8;
    if (size === "sm") textScale = 4;
    if (size === "lg") textScale = 12;
    const textSize = {
        fontSize: `calc(${textScale}px + 1vmin)`
    }
    return (
        <button
            onClick={onClick}
            style={style}
            className={`btn ${className}`}
            data-testid="testButton">
            <div className="btn__border">
                <span className="btn__text" style={textSize} >{label}</span>
            </div>
        </button>
    )
}
Button.propTypes = {
    label: PropTypes.string,
    backgroundColor: PropTypes.string,
    size: PropTypes.oneOf(['sm', 'md', 'lg']),
    onClick: PropTypes.func,
}

export default Button
