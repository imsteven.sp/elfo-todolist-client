import React from 'react'
import './modal.scss'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

const Backdrop = () => {
    return <div className="backdrop" />
}

const ModalOverlay = ({ children }) => {
    return <div className="modal" >
        <div className="__content">{children}</div>
    </div>
}

const portalElement = document.getElementById('overlays');

const Modal = ({ children }) => {
    return (
        <React.Fragment>
            {ReactDOM.createPortal(<Backdrop />, portalElement)}
            {ReactDOM.createPortal(<ModalOverlay>{children}</ModalOverlay>, portalElement)}
        </React.Fragment>
    )
}


Modal.propTypes = {
    children: PropTypes.element,
}
ModalOverlay.propTypes = {
    children: PropTypes.element,
}

export default Modal
