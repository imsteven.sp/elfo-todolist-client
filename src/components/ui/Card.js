import React from 'react'
import PropTypes from 'prop-types'
import './card.scss'

const Card = ({children, className, border, hoverEffect}) => {

    const effect = hoverEffect ? "hoverEffect" : null;

    return (
        <div className={`card ${effect} ${className}`}>
            <div className={border ? 'card__border' : null}>
                {children}
            </div>
        </div>
    )
}

Card.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
    border: PropTypes.bool,
    hoverEffect: PropTypes.bool
}

export default Card
