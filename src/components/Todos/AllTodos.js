import React from 'react';
import SingleTodo from './SingleTodo';
import { useSelector } from 'react-redux';
import Stack from '../ui/Stack';
import './allTodos.scss';
import Button from '../ui/Button'
import { Link } from 'react-router-dom';

const AllTodos = () => {
    const todos = useSelector(state => state.todo.todos)

    return (
        <div className="allTodos">
            <h2>Hi, have a great day !</h2>
            <ul>
                <Stack direction="row">
                    {todos.map((item) => {
                        return (
                            <SingleTodo key={item.id} id={item.id} title={item.title} description={item.description} favourite={item.favourite}></SingleTodo>
                        );
                    })}
                </Stack>
            </ul>
            <Link to='/new'>
                <Button label="+" />
            </Link>
        </div >
    )
}

export default AllTodos
