import React, { useState } from "react";
import Card from "../ui/Card";
import TodoPopup from "./TodoPopup";
import './singleTodo.scss'
import PropTypes from 'prop-types'

const SingleTodo = ({ title, description, id, favourite }) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => {
        setIsOpen(!isOpen)
    }
    return (
        <Card className="pointer" hoverEffect={true}>
            <li>
                <div className="singleTodo" onClick={toggle}>
                    <span className="textTitle">{title}</span>
                    <br />
                    <span className="textDesc">{description}</span>
                </div>
                {isOpen && <TodoPopup id={id} currentTitle={title} currentDescription={description} favourite={favourite} close={toggle} />}
            </li>
        </Card>
    );
}
SingleTodo.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    id: PropTypes.number,
    favourite: PropTypes.number
}

export default SingleTodo;