import React, { useState, useEffect, useRef } from 'react'
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { todoActions } from '../../store/todo-slice';
import Button from '../ui/Button';
import Card from '../ui/Card';
import './newTodo.scss'

const NewTodo = () => {
    const dispatch = useDispatch();
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const inputRef = useRef();

    //POST API
    const postData = async (id, title, description) => {
        await axios.post(`http://localhost:9001/task/create/${id}/${title}/${description}`);
        console.log('Pushing Data from API')
    };

    const onAddHandler = (param1, param2, e) => {
        e.preventDefault()
        const newRandomId = Math.floor(Math.random() * 1000);
        dispatch(todoActions.addTodo({ id: newRandomId, title, description, favourite: false }));
        postData(newRandomId, param1, param2);
        setTitle('');
        setDescription('');
    }

    useEffect(() => {
        inputRef.current.focus();
    }, []);

    return (
        <div className="newtodo">
            <h2>What is in your mind ?</h2>
            <Card border>
                <form onSubmit={(e) => onAddHandler(title, description, e)}>
                    <input
                        className="newtodo__title textTitle"
                        name="title"
                        data-testid="testTitle"
                        ref={inputRef}
                        placeholder="TITLE"
                        required
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}>
                    </input>
                    <br />
                    <textarea
                        className="newtodo__desc textDesc"
                        name="description"
                        data-testid=""
                        placeholder="Write your NOTES here.."
                        value={description}
                        required
                        onChange={(e) => setDescription(e.target.value)}
                        // cols="60"
                        rows="10">
                    </textarea>
                    <div style={{ position:'relative', bottom:'-10rem', display: 'flex', justifyContent: 'center' }}>
                        <Button type="submit" label='ADD'></Button>
                    </div>
                </form>
            </Card>
        </div>
    )
}

export default NewTodo
