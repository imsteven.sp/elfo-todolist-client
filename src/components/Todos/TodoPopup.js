import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import Modal from '../ui/Modal'
import './todoPopup.scss'
import { GrClose } from 'react-icons/gr';
import { todoActions } from "../../store/todo-slice";
import { useDispatch } from "react-redux";
import Button from '@mui/material/Button';
import { ButtonGroup } from "@mui/material";
import SaveIcon from '@mui/icons-material/Save';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import IconButton from '@mui/material/IconButton';
import PropTypes from 'prop-types'

const TodoPopup = ({ currentTitle, currentDescription, id, favourite, close }) => {

    const dispatch = useDispatch();
    const [title, setTitle] = useState(currentTitle)
    const [desc, setDesc] = useState(currentDescription)
    const [edit, setEdit] = useState(false);
    const inputRef = useRef();

    //DELETE API
    const deleteData = async (id) => {
        await axios.delete(`http://localhost:9001/task/delete/${id}`);
        console.log('Deleting Data from API')
    };

    const deleteTodo = i => {
        console.log(i)
        deleteData(i);
        dispatch(todoActions.deleteTodo({ id: i }));
    }

    //UPDATE API
    const updateData = async (id, title, description, favourite) => {
        await axios.put(`http://localhost:9001/task/update/${id}/${title}/${description}/${favourite}`);
        console.log('Updating Data to API into' + favourite)
    };

    const handleEdit = () => {
        setEdit(true);
    }

    const handleSave = () => {
        updateData(id, title, desc);
        dispatch(todoActions.updateTodo({
            id: id,
            title: title,
            description: desc
        }))
        setEdit(false);
    }

    const handleAddFavourite = () => {
        updateData(id, title, desc, 1);
        dispatch(todoActions.addFavourite(id))
    }

    const handleRemoveFavourite = () => {
        updateData(id, title, desc, 0);
        dispatch(todoActions.removeFavourite(id))
    }

    useEffect(() => {
        edit && inputRef.current.focus();
    }, [edit]);

    return (
        <Modal>
            <div className="todoModal">
                <input className="textTitle"
                    type='text'
                    value={title}
                    onChange={e => setTitle(e.target.value)}
                    disabled={!edit}>
                </input>
                <textarea className="todoModal__desc textDesc"
                    type='text'
                    ref={inputRef}
                    value={desc}
                    onChange={e => setDesc(e.target.value)}
                    disabled={!edit}
                    rows="10"
                />
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    {favourite === 1 ?
                        <>
                            <Button className="todoModal__btn" variant="contained" onClick={handleRemoveFavourite} endIcon={<FavoriteIcon />}>
                                Favourite
                            </Button>
                            <IconButton className="todoModal__iconBtn" color="primary" aria-label="favourite" onClick={handleRemoveFavourite}>
                                <FavoriteIcon />
                            </IconButton>
                        </>
                        :
                        <>
                            <Button className="todoModal__btn" variant="outlined" onClick={handleAddFavourite} endIcon={<FavoriteBorderIcon />}>
                                Favourite
                            </Button>
                            <IconButton className="todoModal__iconBtn" color="primary" aria-label="favourite" onClick={handleAddFavourite}>
                                <FavoriteBorderIcon />
                            </IconButton>
                        </>
                    }
                    <ButtonGroup variant="outlined">
                        {edit ?
                            <Button variant="contained" onClick={handleSave} endIcon={<SaveIcon />}>Save</Button>
                            :
                            <Button variant="contained" onClick={handleEdit} endIcon={<EditIcon />}>Edit</Button>
                        }
                        <>
                            <Button className="todoModal__btn" variant="outlined" onClick={() => deleteTodo(id)} endIcon={<DeleteIcon />}>Delete</Button>
                            <IconButton className="todoModal__iconBtn" color="primary" aria-label="favourite" onClick={() => deleteTodo(id)}>
                                <DeleteIcon />
                            </IconButton>
                        </>
                    </ButtonGroup>
                </div>

                <GrClose className="todoModal__close-icon" onClick={close} />

            </div>
        </Modal >
    )
}
TodoPopup.propTypes = {
    currentTitle: PropTypes.string,
    currentDescription: PropTypes.string,
    id: PropTypes.number,
    favourite: PropTypes.number,
    close: PropTypes.func,
}

export default TodoPopup
