import React from 'react';
import SingleTodo from './SingleTodo';
import { useSelector } from 'react-redux';
import Stack from '../ui/Stack';
import './allTodos.scss';
import Button from '../ui/Button'

const FavouriteTodos = () => {
    const favouriteTodos = useSelector(state => state.todo.favTodos)

    return (
        <div className="allTodos">
            <h2>These all are your favourites notes !</h2>
            <ul>
                <Stack direction="row">
                    {favouriteTodos.map((item) => {
                        return (
                            <SingleTodo key={item.id} id={item.id} title={item.title} description={item.description} favourite={item.favourite}></SingleTodo>
                        );
                    })}
                </Stack>
            </ul>
            <Button label="+" />
        </div>
    )
}

export default FavouriteTodos
