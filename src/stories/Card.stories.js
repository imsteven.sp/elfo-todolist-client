import Card from "../components/ui/Card"

export default {
    title: 'Components/Card',
    component: Card,
}

const Template = args => <Card {...args} />

export const Normal = Template.bind({})
Normal.args = {
}
